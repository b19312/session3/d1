package com.zuitt.batch193;

public class Main {

    public static void main(String[] args){

        //For Loops
        for(int i = 0; i < 10; i++){
            System.out.println("Current Count: " + i);
        }

        //with array
        int[] intArray = {100, 200, 300, 400, 500};

        for(int i = 0; i < intArray.length; i++) {

            System.out.println(intArray[i]);

        }

        //Another way to loop through arrays. It is called foreach loop or the enhanced for loop.

        String[] nameArray = {"John", "Paul", "George", "Ringo"};

        for(String name: nameArray){
            System.out.println(name);
        }

        for(int sample: intArray){
            System.out.println(sample);
        }

        //Nested for loops

        String[][] classroom = new String[3][3];

        classroom[0][0] = "Loid";
        classroom[0][1] = "Yor";
        classroom[0][2] = "Anya";

        classroom[1][0] = "Dahyun";
        classroom[1][1] = "Chaeyoung";
        classroom[1][2] = "Naeyon";

        classroom[2][0] = "Luffy";
        classroom[2][1] = "Zorro";
        classroom[2][2] = "Sanji";

        for(int row = 0; row < 3; row++) {//outer loop will loops through the rows

            for(int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }

        //while loops
        int x = 0;
        int y = 10;

        while(x < 10) {
            System.out.println("Loop number: " + x);
            x++;
        }

        //do-while
        do {
            System.out.println("Countdown: " + y);
            y--;
        } while(y > 0);


    }
}
