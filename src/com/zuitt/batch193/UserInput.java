package com.zuitt.batch193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInput {

    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Input a number");

        int num = input.nextInt();

        System.out.println("The number you entered is: " + num);

        //Run-time errors - errors happen after compilation and during the execution of the program

        //Compile-time errors - happen when you try to compile the program that is syntactically incorrect or has a missing package imports

        //Exception handling, manage and catch the run-time errors in order to safely run the code.

        //we handle such situations in Java by using try-catch(optional: Finally)

        int num1 = 0;

        try{
            System.out.println("Please enter a number from 1 to 10");
            num1 = input.nextInt();
        } catch (InputMismatchException e){
            System.out.println("Input is not a number");
        }

        catch (Exception e){//Exception to catch any errors(generic error)
            System.out.println("Invalid Input");
        } finally {
            if(num1 != 0){
                System.out.println("The number you entered is: " + num1);
            }
        }
    }
}
